"use strict";

const Joi = require("joi");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const { findByEmail } = require("../repositories/user-repository");
const createJsonError = require("../errors/create-json-errors");

const schema = Joi.object().keys({
  email: Joi.string().email().required(),
  password: Joi.string().min(4).max(20).required(),
});

async function loginUser(req, res) {
  try {
    await schema.validateAsync(req.body);
    const { email, password } = req.body;
    const user = await findByEmail(email);
    if (!user) {
      const error = new Error("No existe usuario con este email");
      error.code = 401;
      throw error;
    }
    const isValidPassword = await bcrypt.compare(password, user.password);
    if (!isValidPassword) {
      const error = new Error("El campo password no es válido");
      error.code = 401;
      throw error;
    }
    const secret = process.env.JWT_SECRET;

    const jwtTokenExpiration = "30m";

    const token = jwt.sign(email, secret, { expiresIn: jwtTokenExpiration });
    const response = {
      accessToken: token,
      expiresIn: jwtTokenExpiration,
    };
    res.send(response);
  } catch (err) {
    createJsonError(err, res);
  }
}

module.exports = loginUser;
