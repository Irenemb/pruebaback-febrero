"use strict";

const Joi = require("joi");
const bcrypt = require("bcryptjs");
const cryptoRandomString = require("crypto-random-string");

const { createUser, findByEmail } = require("../repositories/user-repository");

const createJsonError = require("../errors/create-json-errors");

const schema = Joi.object().keys({
  email: Joi.string().email().required(),
  password: Joi.string().min(4).max(20).required(),
  repeatPassword: Joi.ref("password"),
});

async function registerUsers(req, res) {
  try {
    await schema.validateAsync(req.body);
    const { email, password } = req.body;
    const existUser = await findByEmail(email);

    if (existUser) {
      const error = new Error("Ya existe un usuario con ese email");
      error.status = 409;
      throw error;
    }
    const passwordHash = await bcrypt.hash(password, 12);
    const userId = await createUser(email, passwordHash);

    const verificationCode = cryptoRandomString({ length: 64 });

    await addVerificationCode(userId, verificationCode);

    res.status(201).send({ email });
  } catch (err) {
    createJsonError(err, res);
  }
}

module.exports = registerUsers;
