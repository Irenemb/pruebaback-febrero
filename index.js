"use strict";

require("dotenv").config();
const express = require("express");
const morgan = require("morgan");
const fs = require("fs");
const path = require("path");
const loginUser = require("./user/loginUser");
const registerUser = require("./user/registerUser");

const app = express();
app.use(express.json());

const port = process.env.SERVER_PORT || 3080;
const accessLogStream = fs.createWriteStream(
  path.join(__dirname, "./accesslog.log"),
  { flags: "a" }
);
app.use(morgan("combined", { stream: accessLogStream }));
app.use("/login", loginUser);
app.use("/register", registerUser);

app.listen(port, () => console.log(`Listening ${port}...`));
