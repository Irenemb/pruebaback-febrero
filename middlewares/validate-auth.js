"use strict";
const jwt = require("jsonwebtoken");
const { JWT_SECRET } = process.env;

function validateAuth(req, res, next) {
  const { authorization } = req.headers;
  try {
    if (!authorization || !authorization.startsWith("Bearer")) {
      const error = new Error();
      error.status = 403;
      error.message = "Authorization required";
      throw error;
    }
    const accessToken = authorization.split(" ")[1];
    const payload = jwt.verify(accessToken, JWT_SECRET);
    req.auth = { email, password };
    next();
  } catch (error) {
    res.status(401);
    res.send(err.message);
  }
}

module.exports = { validateAuth };
