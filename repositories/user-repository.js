"use strict";

const database = {};

const myDataBase = [database];

async function createUser(email, password) {
  const addtoMyDataBase = await myDataBase.push({
    email,
    password,
  });
  database = myDataBase.JSON.stringify;
}

async function findByEmail(email) {
  const emailExists = myDataBase.find((param) => param.email === email);

  if (emailExists) {
    return true;
  }
  return false;
}

module.exports = {
  createUser,
  findByEmail,
};
