"use strict";
const router = require("express").Router();
const registerUser = require("../user/loginUser");
const { validateAuth } = require("../middlewares/validate-auth");

router
  .route("/register")
  .post(async (req, res) => await registerUser.register(req, res));

router
  .route("/login")
  .post(async (req, res) => await registerUser.login(req, res));

router.route("/").all((validateAuth) => {
  res.redirect();
});

module.exports = { router };
